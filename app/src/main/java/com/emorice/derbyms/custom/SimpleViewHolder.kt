package com.emorice.derbyms.custom

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.emorice.derbyms.utils.inflate

/**
 * Created by axl-e.morice on 03/07/2017
 */
class SimpleViewHolder(parent: ViewGroup, layout: Int) : RecyclerView.ViewHolder(parent.inflate(layout))
