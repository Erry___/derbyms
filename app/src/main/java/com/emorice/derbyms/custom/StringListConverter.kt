package com.emorice.derbyms.custom

import io.requery.Converter

/**
 * Created by axl-e.morice on 18/07/2017
 */
class StringListConverter : Converter<ArrayList<String>, String> {
    val separator = '\u0007'

    override fun convertToMapped(type: Class<out ArrayList<String>>?, value: String?): ArrayList<String> {
        if (value == null)
            return ArrayList()
        return ArrayList(value.split(separator))
    }

    override fun convertToPersisted(value: ArrayList<String>?): String {
        if (value == null)
            return ""
        val sb = StringBuilder()
        val size = value.size
        for (i in value.indices) {
            sb.append(value[i])
            if (i < size - 1)
                sb.append(separator)
        }
        return sb.toString()
    }

    override fun getPersistedSize(): Int? = null

    override fun getMappedType(): Class<ArrayList<String>> = ArrayList<String>().javaClass

    override fun getPersistedType(): Class<String> = String::class.java

}