package com.emorice.derbyms.custom

import android.animation.Animator

/**
 * Created by axl-e.morice on 27/06/2017
 */
abstract class SimpleAnimatorListener : Animator.AnimatorListener {
    override fun onAnimationRepeat(animation: Animator?) {
    }

    override fun onAnimationCancel(animation: Animator?) {
    }

    override fun onAnimationStart(animation: Animator?) {
    }
}