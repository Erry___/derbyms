package com.emorice.derbyms.custom

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.emorice.derbyms.R
import com.emorice.derbyms.utils.getFont

/**
 * Created by axl-e.morice on 27/06/2017
 */
class FontTextView : AppCompatTextView {

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initTextview(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        initTextview(attrs)
    }

    private fun initTextview(attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.FontTextView, 0, 0)
        val name = a.getString(R.styleable.FontTextView_ttfNameTextView)
        typeface = context.getFont(name)
        a.recycle()
    }
}