package com.emorice.derbyms.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View

/**
 * Created by axl-e.morice on 19/07/2017
 */
abstract class BaseFragment<P : BasePresenter> : Fragment(), BaseView<P> {
    protected val presenter: P by lazy { createPresenter() }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.init()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detach()
    }
}