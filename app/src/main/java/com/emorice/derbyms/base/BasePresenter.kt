package com.emorice.derbyms.base

/**
 * Created by axl-e.morice on 23/06/2017
 */
interface BasePresenter {
    fun init()
    fun detach()
}