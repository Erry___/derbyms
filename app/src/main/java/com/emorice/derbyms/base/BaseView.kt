package com.emorice.derbyms.base

import android.content.Context

/**
 * Created by axl-e.morice on 23/06/2017
 */
interface BaseView<out P : BasePresenter> {
    fun createPresenter() : P
    fun getContext() : Context
}