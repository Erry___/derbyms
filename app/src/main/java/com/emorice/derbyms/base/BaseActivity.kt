package com.emorice.derbyms.base

import android.support.v7.app.AppCompatActivity

/**
 * Created by axl-e.morice on 23/06/2017
 */
abstract class BaseActivity<P : BasePresenter> : AppCompatActivity(), BaseView<P> {
    protected val presenter: P by lazy { createPresenter() }

    override fun onResume() {
        super.onResume()
        presenter.init()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

    override fun getContext() = this
}