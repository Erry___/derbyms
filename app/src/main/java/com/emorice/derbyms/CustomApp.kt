package com.emorice.derbyms

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import io.fabric.sdk.android.Fabric

/**
 * Created by axl-e.morice on 29/06/2017
 */
class CustomApp : Application() {

    override fun onCreate() {
        super.onCreate()

        Fabric.with(this, Crashlytics())
        if (BuildConfig.DEBUG)
            Stetho.initializeWithDefaults(this)
    }
}