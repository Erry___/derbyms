package com.emorice.derbyms.presenter

import com.emorice.derbyms.contract.DashboardContract
import com.emorice.derbyms.model.local.DatabaseManager
import com.emorice.derbyms.view.QuizzActivity
import com.emorice.derbyms.view.SkillsActivity
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

/**
 * Created by axl-e.morice on 23/06/2017
 */
class DashboardPresenter(val view: DashboardContract.View) : DashboardContract.Presenter {

    companion object {
        const val APP_SCHEME = "derbyTest://"
        const val URL_QUIZZ = APP_SCHEME + "test"
        const val URL_SKATE = APP_SCHEME + "skate"
        const val URL_RULES = APP_SCHEME + "rules"
    }

    var subscriptions = CompositeSubscription()

    override fun navigate(url: String) {
        when (url) {
            URL_QUIZZ -> view.start(QuizzActivity::class.java)
            URL_SKATE -> view.start(SkillsActivity::class.java)
            URL_RULES -> TODO()
        }
    }

    override fun init() {
        val subscription = DatabaseManager(view.getContext())
                .getCurrentScore()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    view.setScore(it.first, it.second)
                }, {
                    it.printStackTrace()
                })
        subscriptions.add(subscription)
    }


    override fun detach() {
        subscriptions.clear()
    }
}