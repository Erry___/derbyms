package com.emorice.derbyms.presenter

import com.emorice.derbyms.adapter.SkatingSkillAdapter
import com.emorice.derbyms.contract.SkillsContract
import com.emorice.derbyms.model.DataManager

/**
 * Created by axl-e.morice on 30/06/2017
 */
class SkillsPresenter(val view: SkillsContract.View) : SkillsContract.Presenter {
    private val skatingSkillAdapter by lazy { SkatingSkillAdapter() }

    override fun init() {
        skatingSkillAdapter.setData(DataManager().getSkatingSkillsList())
        view.setAdapter(skatingSkillAdapter)
    }

    override fun detach() {
    }

}