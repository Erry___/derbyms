package com.emorice.derbyms.presenter

import com.emorice.derbyms.adapter.QuizzAdapter
import com.emorice.derbyms.callback.QuizzCallback
import com.emorice.derbyms.contract.QuizzContract
import com.emorice.derbyms.model.DataManager
import com.emorice.derbyms.model.local.DatabaseManager
import com.emorice.derbyms.model.local.QuizzArchiveEntity
import com.emorice.derbyms.model.remote.QuizzItem
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import java.util.*


/**
 * Created by axl-e.morice on 23/06/2017
 */
class QuizzPresenter(val view: QuizzContract.View) : QuizzContract.Presenter {

    private val subscriptions by lazy { CompositeSubscription() }
    private val quizzAdapter by lazy { QuizzAdapter(callback) }

    private val callback = object : QuizzCallback {
        override fun onRightAnswer(item: QuizzItem) {
            saveItem(item)
            view.showSuccess { goToNextQuestion() }
        }

        override fun onWrongAnswer(item: QuizzItem) {
            saveItem(item)
            view.showError { goToNextQuestion() }
        }
    }

    override fun init() {
        quizzAdapter.setCurrentQuestion(DataManager().getRandomQuizzItem())
        view.setAdapter(quizzAdapter)
    }

    override fun detach() {
        subscriptions.clear()
    }

    fun goToNextQuestion() {
        quizzAdapter.setCurrentQuestion(DataManager().getRandomQuizzItem())
    }

    private fun saveItem(item: QuizzItem) {
        val entity = item.copyInfoTo(QuizzArchiveEntity())
        entity.timestamp = Date().time
        val subscription = DatabaseManager(view.getContext())
                .save(entity)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    DatabaseManager(view.getContext()).cleanArchives()
                }, {
                    view.showToast("error")
                    it.printStackTrace()
                })
        subscriptions.add(subscription)
    }
}