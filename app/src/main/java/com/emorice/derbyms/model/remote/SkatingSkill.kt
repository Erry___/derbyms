package com.emorice.derbyms.model.remote

import com.emorice.derbyms.base.BaseModel
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by axl-e.morice on 30/06/2017
 */
data class SkatingSkill(
        @SerializedName("name")
        @Expose
        val name: String,
        @SerializedName("subskills")
        @Expose
        val subskills: List<SkatingSkill>? = null,
        @SerializedName("note")
        @Expose
        val note: String = "",
        @SerializedName("id")
        @Expose
        val id: Long
) : BaseModel {
    fun getProgress(): Pair<Int, Int> {
        return Pair(50, 100)
    }
}