package com.emorice.derbyms.model.local;

import io.requery.Entity;
import io.requery.Key;

/**
 * Ignore this class : it fix a bug of Requery
 */
@Entity
public class Ignore {
    @Key
    int id;
}
