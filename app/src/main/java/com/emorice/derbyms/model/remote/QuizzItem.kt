package com.emorice.derbyms.model.remote

import com.emorice.derbyms.base.BaseModel
import com.emorice.derbyms.model.local.QuizzArchiveEntity
import com.google.gson.annotations.SerializedName

/**
 * Created by axl-e.morice on 23/06/2017
 */
data class QuizzItem(
        @SerializedName("question")
        val question: String,
        @SerializedName("answers")
        val answers: List<String>,
        @SerializedName("correct")
        val correctAnswer: Int,
        @SerializedName("note")
        val note: String? = null
) : BaseModel {
    var chosenAnswer = -1

    fun copyInfoTo(archive: QuizzArchiveEntity): QuizzArchiveEntity {
        archive.question = question
        archive.answers = ArrayList(answers)
        archive.correctAnswer = correctAnswer
        archive.note = note
        archive.chosenAnswer = chosenAnswer
        return archive
    }
}