package com.emorice.derbyms.model.remote

import com.google.gson.annotations.SerializedName

/**
 * Created by axl-e.morice on 27/06/2017
 */
data class ServerQuizzWrapper (
        @SerializedName("timestamp")
        val timestamp : Long,
        @SerializedName("items")
        val items : List<QuizzItem>
)