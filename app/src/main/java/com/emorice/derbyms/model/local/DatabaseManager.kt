package com.emorice.derbyms.model.local

import android.content.Context
import android.util.Log
import com.emorice.derbyms.BuildConfig
import io.requery.Persistable
import io.requery.android.sqlite.DatabaseSource
import io.requery.rx.KotlinRxEntityStore
import io.requery.sql.KotlinEntityDataStore
import io.requery.sql.TableCreationMode
import rx.Single
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by axl-e.morice on 29/06/2017
 */
class DatabaseManager(val context: Context) {

    private object DataStore {
        val version = 5
        var instance: KotlinRxEntityStore<Persistable>? = null
    }

    private fun getDataStore(): KotlinRxEntityStore<Persistable> {
        val tmp = DataStore.instance
        if (tmp != null)
            return tmp

        val source = DatabaseSource(context, Models.DEFAULT, DataStore.version)
        if (BuildConfig.DEBUG) {
            source.setLoggingEnabled(true)
        }
        source.setTableCreationMode(TableCreationMode.DROP_CREATE)
        val dataStore = KotlinRxEntityStore<Persistable>(KotlinEntityDataStore(source.configuration))
        DataStore.instance = dataStore
        return dataStore
    }

    fun save(quizzArchive: QuizzArchive): Single<QuizzArchive> = getDataStore().upsert(quizzArchive)

    fun cleanArchives() {
        getDataStore().select(QuizzArchiveEntity::class)
                .orderBy(QuizzArchiveEntity.TIMESTAMP.desc())
                .limit(40)
                .get()
                .toObservable()
                .last()
                .flatMap {
                    log("timestamp : " + it.timestamp)
                    return@flatMap getDataStore().delete(QuizzArchiveEntity::class)
                            .where(QuizzArchiveEntity.TIMESTAMP.lt(it.timestamp))
                            .get()
                            .toSingle()
                            .toObservable()
                }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ log("I deleted " + it + " items") }, { it.printStackTrace() })
    }

    fun getCurrentScore() : Single<Pair<Int, Int>> = getDataStore()
            .count(QuizzArchiveEntity::class)
            .where(QuizzArchiveEntity.CORRECT_ANSWER.eq(QuizzArchiveEntity.CHOSEN_ANSWER))
            .get().toSingle()
            .flatMap {
                val count = it
                getDataStore().count(QuizzArchiveEntity::class)
                        .get().toSingle()
                        .map { return@map Pair(count, it) }
            }

    fun log(msg: String) {
        Log.i("DataBaseManager", msg)
    }
}