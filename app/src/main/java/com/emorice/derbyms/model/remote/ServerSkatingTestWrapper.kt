package com.emorice.derbyms.model.remote

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by axl-e.morice on 30/06/2017
 */
data class ServerSkatingTestWrapper(
        @SerializedName("timestamp")
        @Expose
        val timestamp: Long,
        @SerializedName("definitions")
        @Expose
        val definitions: List<String>,
        @SerializedName("suppressOlder")
        @Expose
        val suppressOlder: Boolean = false,
        @SerializedName("skills")
        @Expose
        val skills: List<SkatingSkill>
)