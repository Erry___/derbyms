package com.emorice.derbyms.model

import android.content.Context
import com.emorice.derbyms.model.remote.QuizzItem
import com.emorice.derbyms.model.remote.ServerQuizzWrapper
import com.emorice.derbyms.model.remote.ServerSkatingTestWrapper
import com.emorice.derbyms.model.remote.SkatingSkill
import com.emorice.derbyms.utils.fileAsString
import com.google.gson.Gson
import java.util.*

/**
 * Created by axl-e.morice on 23/06/2017
 */
class DataManager {

    companion object {
        private var quizzItems: List<QuizzItem> = listOf()
        private var skatingSkills: List<SkatingSkill> = listOf()

        fun init(context: Context) {
            val fileContent = context.assets.fileAsString("theorie.json")
            val wrapper = Gson().fromJson(fileContent, ServerQuizzWrapper::class.java)
            quizzItems = wrapper.items
            val skatingFileContent = context.assets.fileAsString("pratique.json")
            val skatingWrapper = Gson().fromJson(skatingFileContent, ServerSkatingTestWrapper::class.java)
            skatingSkills = skatingWrapper.skills
        }
    }

    fun getRandomQuizzItem(): QuizzItem {

        if (quizzItems.isNotEmpty()) {
            val random = Random().nextInt(quizzItems.size)
            return quizzItems[random]
        }

        val answer = Random().nextInt(3)
        val list = mutableListOf("Faux", "Faux", "Faux")
        list[answer] = "Correct"

        return QuizzItem(Date().time.toString() + " - Choisissez la réponse", list, answer)
    }

    fun getSkatingSkillsList() = skatingSkills
}