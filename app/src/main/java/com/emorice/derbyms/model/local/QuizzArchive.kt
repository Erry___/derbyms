package com.emorice.derbyms.model.local

import com.emorice.derbyms.custom.StringListConverter
import io.requery.*

/**
 * Created by axl-e.morice on 29/06/2017
 */
@Entity
interface QuizzArchive : Persistable {
    @get:Key
    @get:Generated
    val id: Int

    @get:Index
    var timestamp: Long

    var question: String

    @get:Convert(StringListConverter::class)
    var answers: ArrayList<String>

    var correctAnswer: Int
    var note: String?
    var chosenAnswer: Int
}