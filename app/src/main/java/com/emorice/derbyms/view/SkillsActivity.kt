package com.emorice.derbyms.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.emorice.derbyms.R
import com.emorice.derbyms.adapter.SkatingSkillAdapter
import com.emorice.derbyms.base.BaseActivity
import com.emorice.derbyms.contract.SkillsContract
import com.emorice.derbyms.presenter.SkillsPresenter
import kotlinx.android.synthetic.main.activity_skills.*

/**
 * Created by axl-e.morice on 30/06/2017
 */
class SkillsActivity : BaseActivity<SkillsContract.Presenter>(), SkillsContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skills)

        skating_recyclerview.layoutManager = LinearLayoutManager(this)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun createPresenter() = SkillsPresenter(this)

    override fun setAdapter(adapter: SkatingSkillAdapter) {
        skating_recyclerview.adapter = adapter
    }
}