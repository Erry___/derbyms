package com.emorice.derbyms.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.emorice.derbyms.R
import com.emorice.derbyms.base.BaseFragment
import com.emorice.derbyms.contract.LeadTheoryContract
import com.emorice.derbyms.presenter.LeadTheoryPresenter

/**
 * Created by axl-e.morice on 19/07/2017
 */
class LeadTheoryFragment : BaseFragment<LeadTheoryContract.Presenter>(), LeadTheoryContract.View {

    companion object {
        fun newInstance(): LeadTheoryFragment = LeadTheoryFragment()
    }

    override fun createPresenter() = LeadTheoryPresenter()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_theory, container, false)
    }

}
