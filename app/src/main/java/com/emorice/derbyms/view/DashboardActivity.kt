package com.emorice.derbyms.view

import android.app.Activity
import android.os.Bundle
import com.emorice.derbyms.R
import com.emorice.derbyms.base.BaseActivity
import com.emorice.derbyms.contract.DashboardContract
import com.emorice.derbyms.model.DataManager
import com.emorice.derbyms.presenter.DashboardPresenter
import com.emorice.derbyms.utils.launchActivity
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity<DashboardContract.Presenter>(), DashboardContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_dashboard)

        dashboard_quizz_card.setOnClickListener { presenter.navigate(DashboardPresenter.URL_QUIZZ) }
        dashboard_skating_card.setOnClickListener { presenter.navigate(DashboardPresenter.URL_SKATE) }
        dashboard_rule_card.setOnClickListener { presenter.navigate(DashboardPresenter.URL_RULES) }

        DataManager.init(this)

    }

    override fun createPresenter(): DashboardContract.Presenter = DashboardPresenter(this)


    override fun setScore(first: Int?, second: Int?) {
        if (first != null && second != null && second != 0)
            dashboard_quizz_detail.text = getString(R.string.dashboard_quizz_detail, first, second)
    }

    override fun <T : Activity> start(activity: Class<T>) {
        launchActivity(activity)
    }
}
