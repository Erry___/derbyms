package com.emorice.derbyms.view

import android.animation.Animator
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.animation.DecelerateInterpolator
import com.emorice.derbyms.R
import com.emorice.derbyms.adapter.QuizzAdapter
import com.emorice.derbyms.base.BaseActivity
import com.emorice.derbyms.contract.QuizzContract
import com.emorice.derbyms.custom.SimpleAnimatorListener
import com.emorice.derbyms.presenter.QuizzPresenter
import com.emorice.derbyms.utils.toast
import com.sackcentury.shinebuttonlib.ShineButton
import kotlinx.android.synthetic.main.activity_quizz.*

/**
 * Created by axl-e.morice on 23/06/2017
 */
class QuizzActivity : BaseActivity<QuizzContract.Presenter>(), QuizzContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quizz)

        quizz_recyclerview.layoutManager = LinearLayoutManager(this)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun createPresenter()= QuizzPresenter(this)

    override fun setAdapter(quizzAdapter: QuizzAdapter) {
        quizz_recyclerview.adapter = quizzAdapter
    }

    override fun showToast(message: String) {
        toast(message)
    }

    override fun showError(next: () -> Unit) {
        animateShineButton(quizz_wronganswer_indicator, next)
    }

    override fun showSuccess(next: () -> Unit) {
        animateShineButton(quizz_correctanswer_indicator, next)
    }

    fun animateShineButton(button: ShineButton, next: () -> Unit) {
        button.isChecked = false
        button.visibility = View.VISIBLE
        quizz_hiding_panel.alpha = 0f
        quizz_hiding_panel.visibility = View.VISIBLE
        quizz_hiding_panel.animate()
                .alpha(0.8f)
                .setDuration(700)
                .setInterpolator(DecelerateInterpolator())
                .setListener(object : SimpleAnimatorListener() {
                    override fun onAnimationEnd(animation: Animator?) {
                        quizz_hiding_panel.alpha = 0.8f
                    }
                })
        button.performClick()
        button.postDelayed({
            button.visibility = View.GONE
            quizz_hiding_panel.visibility = View.GONE
            next()
        }, 1000)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}