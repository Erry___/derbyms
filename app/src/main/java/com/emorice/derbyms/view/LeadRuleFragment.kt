package com.emorice.derbyms.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.emorice.derbyms.R
import com.emorice.derbyms.base.BaseFragment
import com.emorice.derbyms.contract.LeadRuleContract
import com.emorice.derbyms.presenter.LeadRulePresenter

/**
 * Created by axl-e.morice on 19/07/2017
 */
class LeadRuleFragment : BaseFragment<LeadRuleContract.Presenter>(), LeadRuleContract.View  {

    companion object {
        fun newInstance(): LeadRuleFragment = LeadRuleFragment()
    }

    override fun createPresenter() = LeadRulePresenter()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_rules, container, false)
    }
}