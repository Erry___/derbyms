package com.emorice.derbyms.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.emorice.derbyms.R
import com.emorice.derbyms.callback.QuizzCallback
import com.emorice.derbyms.custom.SimpleViewHolder
import com.emorice.derbyms.model.remote.QuizzItem
import com.emorice.derbyms.utils.inflate
import com.emorice.derbyms.utils.setColor
import com.emorice.derbyms.utils.setGone
import com.emorice.derbyms.utils.setVisible
import kotlinx.android.synthetic.main.item_button.view.*
import kotlinx.android.synthetic.main.item_quizz_archive.view.*
import kotlinx.android.synthetic.main.item_quizz_question.view.*
import kotlinx.android.synthetic.main.item_separator.view.*

/**
 * Created by axl-e.morice on 23/06/2017
 */
class QuizzAdapter(val callback: QuizzCallback) : RecyclerView.Adapter<SimpleViewHolder>() {

    object ViewType {
        val SEPARATOR = 0
        val QUESTION = 1
        val ARCHIVE = 2
    }

    private var currentItem: QuizzItem? = null
    private var archives: MutableList<QuizzItem> = mutableListOf()

    fun setCurrentQuestion(item: QuizzItem) {
        val old = currentItem

        currentItem = item
        notifyItemChanged(0)

        if (old != null) {
            archives.add(old)
            if (archives.size < 2)
                notifyItemInserted(1)
            else
                notifyItemChanged(1)
            notifyItemInserted(2)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder = when (viewType) {
        ViewType.QUESTION -> SimpleViewHolder(parent, R.layout.item_quizz_question)
        ViewType.SEPARATOR -> SimpleViewHolder(parent, R.layout.item_separator)
        ViewType.ARCHIVE -> SimpleViewHolder(parent, R.layout.item_quizz_archive)
        else -> TODO()
    }


    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        when (getItemViewType(position)) {
            ViewType.QUESTION -> bindQuestion(holder.itemView, currentItem)
            ViewType.SEPARATOR -> bindSeparator(holder.itemView)
            ViewType.ARCHIVE -> bindArchive(holder.itemView, archives[archives.size - position + 1])
        }
    }

    fun bindQuestion(view: View, item: QuizzItem?) = with(view) {
        if (item != null) {
            quizzitem_question.text = item.question
            quizzitem_answers_layout.removeAllViews()
            for (i in item.answers.indices) {
                val childView = quizzitem_answers_layout.inflate(R.layout.item_button, false)
                with(childView) {
                    buttonText.text = item.answers[i]
                    button.setOnClickListener {
                        item.chosenAnswer = i
                        if (i == item.correctAnswer)
                            callback.onRightAnswer(item)
                        else
                            callback.onWrongAnswer(item)
                    }
                }
                quizzitem_answers_layout.addView(childView)
            }
        } else {
            // TODO : un loading, peut être ?
        }
    }

    fun bindSeparator(view: View) = with(view) {
        quizz_separator_score.text = resources.getString(R.string.quizz_score, archives.count { it.chosenAnswer == it.correctAnswer }, archives.size)
    }

    fun bindArchive(view: View, item: QuizzItem) = with(view) {
        quizzitem_archive_question.text = item.question
        for (i in item.answers.indices) {
            if (i == item.chosenAnswer && i == item.correctAnswer) {
                quizzitem_answer.text = resources.getString(R.string.quizz_archive_answer, item.answers[i])
                quizzitem_answer.setColor(R.color.quizz_answer_correct)
                quizzitem_wrong_answer.setGone()
            } else if (i == item.chosenAnswer) {
                quizzitem_wrong_answer.setVisible()
                quizzitem_wrong_answer.text = resources.getString(R.string.quizz_archive_your_answer_wrong, item.answers[i])
            } else if (i == item.correctAnswer) {
                quizzitem_answer.text = resources.getString(R.string.quizz_archive_answer, item.answers[i])
                quizzitem_answer.setColor(R.color.textColor)
            }
        }
        if (item.note != null) {
            quizzitem_note.text = resources.getString(R.string.quizz_archive_note, item.note)
            quizzitem_note.setVisible()
        } else {
            quizzitem_note.setGone()
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0)
            return ViewType.QUESTION
        if (position == 1)
            return ViewType.SEPARATOR
        return ViewType.ARCHIVE
    }

    override fun getItemCount(): Int {
        if (archives.isEmpty())
            return 1
        return archives.size + 2
    }
}