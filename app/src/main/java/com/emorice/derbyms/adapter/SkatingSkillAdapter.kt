package com.emorice.derbyms.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.emorice.derbyms.R
import com.emorice.derbyms.custom.SimpleViewHolder
import com.emorice.derbyms.model.remote.SkatingSkill
import kotlinx.android.synthetic.main.item_skill_main.view.*

/**
 * Created by axl-e.morice on 03/07/2017
 */
class SkatingSkillAdapter : RecyclerView.Adapter<SimpleViewHolder>() {

    private var data: List<SkatingSkill> = mutableListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SimpleViewHolder(parent, R.layout.item_skill_main)

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) = with(holder.itemView) {
        val item = data[position]
        val progress = item.getProgress()
        mainskillitem_name.text = item.name
        mainskillitem_percent.text = context.getString(R.string.skating_progress_percent, progress.first * 100 / progress.second)
        mainskillitem_progress.max = progress.second
        mainskillitem_progress.progress = progress.first
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setData(skatingSkillsList: List<SkatingSkill>) {
        data = skatingSkillsList
    }

}