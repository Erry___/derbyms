package com.emorice.derbyms.callback

import com.emorice.derbyms.model.remote.QuizzItem

/**
 * Created by axl-e.morice on 26/06/2017
 */
interface QuizzCallback {
    fun onRightAnswer(item : QuizzItem)
    fun onWrongAnswer(item : QuizzItem)
}