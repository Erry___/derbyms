package com.emorice.derbyms.utils

import android.content.Context
import android.graphics.Typeface

/**
 * Created by axl-e.morice on 27/06/2017
 */
class FontUtils {
    companion object {
        val listFont: HashMap<String, Typeface> = hashMapOf()
    }
}

fun Context.getFont(ttfName: String): Typeface? {
    if (FontUtils.listFont.containsKey(ttfName)) {
        val font = FontUtils.listFont[ttfName]
        if (font != null)
            return font
    }
    FontUtils.listFont.put(ttfName, Typeface.createFromAsset(assets, "fonts/" + ttfName))
    return FontUtils.listFont[ttfName]
}
