package com.emorice.derbyms.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.AssetManager
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import java.nio.charset.Charset

/**
 * Created by axl-e.morice on 23/06/2017
 */
fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

fun TextView.setColor(id: Int) {
    setTextColor(ContextCompat.getColor(context, id))
}

fun TextView.setVisible() {
    visibility = View.VISIBLE
}

fun TextView.setGone() {
    visibility = View.GONE
}

fun AssetManager.fileAsString(filename: String): String {
    return open(filename).use {
        it.readBytes().toString(Charset.defaultCharset())
    }
}

fun <T : Activity> Context.launchActivity(activity: Class<T>) {
    startActivity(Intent(this, activity))
}

fun Context.toast(message: CharSequence) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()