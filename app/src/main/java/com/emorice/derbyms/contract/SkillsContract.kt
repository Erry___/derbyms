package com.emorice.derbyms.contract

import com.emorice.derbyms.adapter.SkatingSkillAdapter
import com.emorice.derbyms.base.BasePresenter
import com.emorice.derbyms.base.BaseView

/**
 * Created by axl-e.morice on 30/06/2017
 */
interface SkillsContract {
    interface View : BaseView<Presenter> {
        fun setAdapter(adapter: SkatingSkillAdapter)
    }

    interface Presenter : BasePresenter
}