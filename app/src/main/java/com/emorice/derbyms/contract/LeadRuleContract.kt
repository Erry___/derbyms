package com.emorice.derbyms.contract

import com.emorice.derbyms.base.BasePresenter
import com.emorice.derbyms.base.BaseView

/**
 * Created by axl-e.morice on 19/07/2017
 */
interface LeadRuleContract {
    interface View : BaseView<Presenter>

    interface Presenter : BasePresenter
}