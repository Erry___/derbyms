package com.emorice.derbyms.contract

import android.app.Activity
import com.emorice.derbyms.base.BasePresenter
import com.emorice.derbyms.base.BaseView

/**
 * Created by axl-e.morice on 23/06/2017
 */
interface DashboardContract {
    interface View : BaseView<Presenter> {
        fun <T : Activity> start(activity: Class<T>)
        fun setScore(first: Int?, second: Int?)
    }

    interface Presenter : BasePresenter {
        fun navigate(url: String)
    }
}