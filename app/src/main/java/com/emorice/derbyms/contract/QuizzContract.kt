package com.emorice.derbyms.contract

import com.emorice.derbyms.adapter.QuizzAdapter
import com.emorice.derbyms.base.BasePresenter
import com.emorice.derbyms.base.BaseView

/**
 * Created by axl-e.morice on 23/06/2017
 */
interface QuizzContract {
    interface View : BaseView<Presenter> {
        fun setAdapter(quizzAdapter: QuizzAdapter)
        fun showToast(message: String)
        fun showError(next: () -> Unit)
        fun showSuccess(next: () -> Unit)
    }

    interface Presenter : BasePresenter
}